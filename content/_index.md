---
title: "Software Defined Vehicle"
seo_title: "Software Defined Vehicle"
headline: "Software Defined Vehicle"
tagline: "An open technology platform for the software defined vehicle of the future; focused on accelerating innovation of automotive-grade in-car software stacks using open source and open specifications developed by a vibrant community."
date: 2021-05-04T10:00:00-04:00
layout: "single"
hide_breadcrumb: true
hide_sidebar: true
hide_page_title: true
show_featured_story: true
links: [[href: "#members", text: "Get Involved"],[href: "https://accounts.eclipse.org/mailing-list/sdv-wg", text: "Mailing List"]]
---

{{< members >}}

{{< sponsors >}}

{{< grid/div class="featured-section featured-news-events row" isMarkdown="false"  >}}
  {{< grid/div class="container" isMarkdown="false" >}}
    {{< grid/div class="row" isMarkdown="false" >}}
      {{< grid/div class="col-sm-12 featured-news-block" isMarkdown="false" >}}
        {{< grid/div class="featured-news" isMarkdown="false" >}}
          <h2>News</h2>
          {{< newsroom/news id="news-template-id" templateId="custom-news-template" templatePath="/js/templates/news-home.mustache" id="news-list-container" containerClass="match-height-item-by-row" count="2" class="news-list-custom" publishTarget="sdv" includeList="true">}}
        {{</ grid/div >}}
      {{</ grid/div >}}
      {{< grid/div class="col-sm-12 text-center featured-events-block" isMarkdown="false" >}}
        {{< grid/div class="featured-events" isMarkdown="false" >}}
          <h2>Upcoming Events</h2>
          {{< newsroom/events
            id="event-list-container"
            publishTarget="sdv"
            class="events-list-custom"
            containerClass="match-height-item-by-row"
            upcoming="1"
            templateId="custom-events-template"
            templatePath="/js/templates/event-list-format.mustache"
            includeList="true"
            count="4"
          >}}
        {{</ grid/div >}}
      {{</ grid/div >}}
    {{</ grid/div >}}
   {{</ grid/div >}}
{{</ grid/div >}}

{{< about >}}