---
title: Venue
weight: 1
---

The venue for SDV Community Day is the Microsoft office in Lisbon

--

Rua do Fogo de Santelmo, Lot 2.07.02

1990 – 110 Lisbon (next to the Oceanarium)

Phone: (351) 210 491 000

Fax: (351) 210 491 999

Microsoft Customer Care: 808223242