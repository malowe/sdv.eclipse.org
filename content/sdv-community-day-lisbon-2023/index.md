---
title: SDV Community Day Lisbon 2023
headline: ''
custom_jumbotron: |
    <h1 class="event-title text-center">SDV Community Day Lisbon - March 2023</h1>
    <p class="event-subtitle text-center">
        Meet and engage with the thriving SDV Community and work towards building an SDV Distribution entirely consisting of Open Source Projects.
    </p>
    <div class="more-detail jumbotron-register-container">
        <div class="more-detail-text padding-top-40">
            <p>Hybrid Event | Mar 29-30, 2023</p>
            <p class="data-location">Microsoft Office | Lisbon, Portugal</p>       
            <p class="small">(in-person attendance is recommended)</p>
            <ul class="list-inline">
                <li class="margin-top-10"><a class="btn btn-light btn-lg" href="https://www.eventbrite.com/e/sdv-community-day-lisbon-march-2023-tickets-520519135747">Register Now</a></li>
            </ul> 
        </div>
    </div>
hide_page_title: true
hide_headline: true
hide_sidebar: true
hide_breadcrumb: true
header_wrapper_class: header-sdv-contribution-day-2022 header-lisbon-2023 header-overlay
container: container-fluid sdv-contribution-day-2022-container
summary: First SDV community meeting for the year. See how "code first" becomes operational.
layout: single
resources:
    - src: "*.md"
---

<!-- About the Event -->
{{< grid/section-container id="registration" containerClass="padding-bottom-40 padding-top-40" >}}
    {{< grid/div class="container text-center" isMarkdown="false" >}}
        {{< events/registration event="sdv-community-day-lisbon-2023" >}}
        {{</ events/registration >}}
        {{< previous_event_videos >}}
    {{</ grid/div >}}
{{</ grid/section-container >}}

<!--- Agenda -->
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda" >}}
    {{< pages/sdv-community-day-2023/agenda event="sdv-community-day-lisbon-2023" >}}
{{</ grid/section-container >}}
{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}

<!-- Onsite -->
{{< grid/section-container containerClass="container" class="row text-center padding-bottom-60 padding-top-40" isMarkdown="false" >}}
    <h2>Information for Onsite Attendees</h2>
    <img class="lisbon-2023-onsite-image" src="./images/microsoft-office-lisbon.jpg" alt="Microsoft's Lisbon Office"></img>
    {{< info_list resource="onsite-info/*.md" >}}
{{</ grid/div >}}

<!-- Hosted By -->
{{< grid/div class="row grey-row text-center padding-bottom-60 padding-top-40" isMarkdown="true" >}}

## Hosted by Microsoft 

SDV Community Day is organized by the Eclipse Software Defined Vehicle Working Group and hosted by Microsoft. 

{{</ grid/div >}}

<!-- Participating Member Companies -->
{{< grid/section-container containerClass="" class="row grey-row text-center padding-bottom-60 padding-top-40" isMarkdown="false" >}}
    <h2>Thanks to Our Participating Member Companies</h2>
    <ul class="eclipsefdn-members-list list-inline margin-30 flex-center gap-40" data-ml-wg="sdv" data-ml-template="only-logos"></ul>
{{</ grid/section-container >}}

