---
title: "Events"
description: "sdv.eclipse.org Events"
keywords: ["Software Defined Vehicle events"]
layout: "events"
container: "container"
custom_sidebar:
    <a href="https://calendar.google.com/calendar/u/0?cid=Y18yYW1waTJibW9rYTNxdGVyNGRjZWFwMWQ1Z0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t" target="_blank" class="btn btn-primary">
        <i class="fa fa-calendar margin-right-5"></i> Add to your Calendar
    </a>
aliases:
    - /events
---

{{< grid/div class="row" isMarkdown="false" >}}
    {{< newsroom/events publishTarget="sdv" count="10" paginate="true" archive="1" upcoming="1" sortOrder="asc">}}
{{</ grid/div >}}