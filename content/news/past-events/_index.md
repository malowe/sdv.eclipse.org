---
title: "Past Events"
description: "sdv.eclipse.org Past Events"
keywords: ["Software Defined Vehicle past events"]
layout: "events"
container: "container"
custom_sidebar:
    <a href="https://calendar.google.com/calendar/u/0?cid=Y18yYW1waTJibW9rYTNxdGVyNGRjZWFwMWQ1Z0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t" target="_blank" class="btn btn-primary">
        <i class="fa fa-calendar margin-right-5"></i> Add to your Calendar
    </a>
aliases:
    - /past-events
---

{{< grid/div class="row" isMarkdown="false" >}}
    {{< newsroom/events publishTarget="sdv" count="10" paginate="true" archive="1" past_event="true">}}
{{</ grid/div >}}